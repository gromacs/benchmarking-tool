# Summary on Currently Implemented Benchmarking tool for `GROMACS`
The idea of this project was to implement a automated benchmark tool for `GROMACS` that should generate detailed report regarding a `GROMACS` test that includes for example, details of the machine and `gmx` binary along with in-depth performance report from `GROMACS` output and sanity checking.

## Implemented / Done
#### Automation
A command line interface (cli) has been added for automation purpose. More info on automation can be found in `USECASES.md`

#### Running on Cluster
Current implementation support testing on cluster both interactively and using the queue system.

#### Report file
A machine readable (`JSON` formatted) file is generated for each successful test. Content of the report file are:

- Machine info
    - CPU info
        - Brand info
    - GPU info
        - Card info
- Binaries info
    - `GROMACS` binary
        - Version
        - Precision
        - SIMD
        - Combined hash of `GROMACS` binary and `libgromacs`
- Compilers info
    - C
        - Version
        - Flags
    - C++
        - Version
        - Flags
    - Cuda
        - Version
        - Flags
- Libraries info
    - FFTW
    - MPI
- Input info
    - Integrator
    - dt
    - nsteps
- `GROMACS` log info
    - steps override
    - nstlist
    - bonded kernel
    - pme grid
    - update groups
- Performance info
    - `ns/day`

#### Sanity Checking
A benchmark test is considered to be successful if the test passes all the sanity checking and only after that a detailed report will be generated for the test. Current implementation supports the following two sanity checking:

1. The existance of `Finished mdrun` string in `GROMACS` log file.
2. The Wall-time is reasonably long, e.g. > 30 sec.

#### Data aggregation
A separate module added to support data aggregation. The module accepts a list of input directories and an output directory through command line argument to aggregate the data to the provided output directory.

## Not Implemented / Future Work
- [Pascal Merz's](https://github.com/ptmerz/gmxbenchmark) parser can be integrated to fully parse performance table.
- `aggregate` module can be used to set up a `cron` job to collect and aggregate data locally.
- The tool can be extented to create a `tarball` for each test result and unpack the tarball to aggregate the results to a specified result directory.
- Adding `conserved quantity drift` (which will be added to the `2021 release`) to report file.


## Plan regarding prototype installation  
Prototype currently run from source code using real hardware. I didn't consider about VM. But I thought about containerization of the tool and found out that it would be hard to implement (might not be impossible). For example, if we have containerized benchmark tool and would like to test a result using the queue system of the cluster,  we need to submit the job to queue system from within the container to the host of the container. At this moment, I really have no idea how to do that.
