'''
    Author:
        * Muhammed Ahad <ahad3112@yahoo.com, maaahad@gmail.com, maaahad@kth.se>
    Desctiption:
        This module format output
'''

import json
import os
import glob


test_style = ('pre{margin: 20px auto; padding: 20px 0 0 20px;}'
         '')

report_html_template = '''
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>GROMACS Benchmark Report for $id$</title>
    <style>
        $style$
    </style>
  </head>
  <body>
    <h1>GROMACS Benchmark Report for $id$</h1>
    <hr>
    $contents$
  </body>
</html>
'''

index_style = ('table{position: relative; width: 100%; border-collapse: collapse; border: 1px solid, black;}'
               'tr{border: 1px solid black;}'
               'td, th{border: 1px solid black; padding: 10px;}'
               'table caption{font-size: 30px; font-weight: bold;margin-bottom: 10px;}')
index_html_template = '''
<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Gromacs Benchmark Report</title>
    <style>
        $style$
    </style>
  </head>
  <body>
    <table>
      <caption>Gromacs Benchmark Tests</caption>
      <thead>
        <tr>
          <th>Test ID</th>
          <th>Test Time</th>
          <th>GROMACS Version</th>
          <th>Descripiton</th>
        </tr>
      </thead>
      <tbody>
        $contents$
      </tbody>
    </table>
  </body>
</html>
'''


def format_performance_dict(pdict, indent=0, start='', bold_key=False):
    result = start
    line_format = '{0}- {1:<}: {2:<}\n'
    for key in pdict:
        KEY = key.upper() if bold_key else key

        if isinstance(pdict[key], dict):
            result += line_format.format(' ' * indent, KEY, '') + format_performance_dict(pdict[key], indent + 3, bold_key=False)
        else:
            val = pdict[key] if pdict[key] else '[]'
            if isinstance(val, list):
                val = str(val)
            result += line_format.format(' ' * indent, KEY, val)

    return result


def create_report_html(file_name, report, id):
    report_html = report_html_template.replace(
        '$contents$',
        '<pre>{0}</pre>'.format(format_performance_dict(pdict=report, bold_key=True))
    ).replace('$style$', test_style).replace(
        '$id$',
        id
    )

    with open(file_name, 'w') as file:
        file.write(report_html)

def create_anchor(link, test_id):
    return '<a href="{link}">{test_id}</a>'.format(
        link=link,
        test_id=test_id
    )

def create_td(test_link, data, create_link=False):
    if create_link:
        return '<td>{0}</td>\n'.format(
            create_anchor(test_link, data)
        )
    else:
        return '<td>{0}</td>\n'.format(data)

def create_tr(test_link, test_id, test_time, gmx_version, descr):
    tr = '<tr>\n{tds}</tr>\n'
    tds = ''
    create_link = True
    for td in (test_id, test_time, gmx_version, descr):
        tds += create_td(test_link, td, create_link)
        create_link = False

    return tr.format(tds=tds)

def create_index_html(root_dir):
    # We need to use relative path in anchor
    index_html_dir = os.path.join( root_dir,'statichtml')
    tests_html_dir = os.path.join(index_html_dir, 'tests')
    index_html = os.path.join(index_html_dir, 'index.html')
    contents = []
    tests_html = os.listdir(tests_html_dir)
    for test_html in tests_html:
        test_id = os.path.splitext(test_html)[0]
        test_report = get_test_json(root_dir, test_id)
        if test_report:
            contents.append(
                create_tr(
                    './tests/{0}.html'.format(test_id),
                    test_report.get('meta', {}).get('id', 'Not available'),
                    test_report.get('meta', {}).get('time', 'Not available'),
                    test_report.get('binaries', {}).get('gmx', {}).get('version', 'Not available'),
                    test_report.get('meta', {}).get('name', 'Not available'),
                )
            )

    with open(index_html, 'w') as file:
        file.write(
             index_html_template.replace(
                '$contents$',
                ''.join(contents)
            ).replace('$style$', index_style)
        )



def get_test_json(root, test_id):
    reports = glob.glob(os.path.join(root, 'reports/*/*/*/*'))
    test_name = test_id + '.json'
    for report in reports:
        if report.endswith(test_name):
            test_report = json.load(open(report))
            return test_report

    return {}
