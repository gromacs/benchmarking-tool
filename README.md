# Benchmarking Tool for GROMACS
---

## Dependencies

* `Python 3.6 of above`
* [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html)

###### Installation of [ReFrame](https://reframe-hpc.readthedocs.io/en/stable/index.html) with `pip`
    pip install reframe-hpc

## Using the tool
Currently the tool has to be run using source code. More details on how to use it can be found in `docs/USECASES.md`. Information on what features are available and what are under consideration can be found in `docs/SUMMARY.md`.
